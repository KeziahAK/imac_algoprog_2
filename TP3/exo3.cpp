#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value = value;
        this->left = NULL;
        this->right =NULL;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        if (value<this->value){
            
            if (this->left==NULL){
                this->left = new SearchTreeNode(value);
            }
            else{
                this->left-> insertNumber(value);
            }


        }
        else{

            if (this->right==NULL){
                this->right= new SearchTreeNode(value);
            }
            else {
                this->right->insertNumber(value);
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        if (this->left==NULL && this->right==NULL){
            return 1;
        }
        
        int right_height=0;
        int left_height=0;

        if (this->left!=NULL){
            left_height=left->height();
        }

        if (this->right!=NULL){
            right_height=right->height();
        }

        if (left_height > right_height){
            return left_height + 1;
        }

        else{
            return right_height + 1;
        }
        


    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        int sum_nodes_right = 0;
        int sum_nodes_left = 0;


        if(this->isLeaf()){
               return 1;
        }
        else {
               if(this->right!=NULL){
                        sum_nodes_right += this->right->nodesCount();
                    }
                    if(this->left!=NULL){
                       sum_nodes_left  += this->left->nodesCount();
                    }
                    return sum_nodes_right+sum_nodes_left +1;
                }


	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if (this->right==NULL && this->left ==NULL){
            return true;
        }

        return false;
        
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(this->isLeaf()){
            leaves[leavesCount]=this;
            leavesCount++;
        }
        else{
            if(this->left != NULL){
                this->left->allLeaves(leaves, leavesCount);
            }
            if(this->right != NULL){
                this->right->   allLeaves(leaves, leavesCount);
            }
        }
	}


	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        //inorder= au milieu +  commence à gauche (ordre croissant)

        if(this->left != NULL){
            this->left->inorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount]= this;
        nodesCount=nodesCount+1; 

        if(this->right !=NULL){
            this->right->inorderTravel(nodes, nodesCount);
        }

	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        //preorder au début 
        nodes[nodesCount]= this;
        nodesCount=nodesCount+1;
        
        if(this->left != NULL){
            this->left->preorderTravel(nodes,nodesCount);
        }

        if(this->right != NULL){
            this->right-> preorderTravel(nodes,nodesCount);
        }
        
	}


	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        //postorder à la fin

        if(this->left != NULL){
            this->left->postorderTravel(nodes,nodesCount);
        }

        if(this->right != NULL){
            this->right->postorderTravel(nodes,nodesCount);
        }

        nodes[nodesCount]=this;
        nodesCount=nodesCount+1;

	}


	Node* find(int value) {
        // find the node containing value
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
