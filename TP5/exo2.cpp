#include <QApplication>
#include <QString>
#include <time.h>
#include <stdio.h>
#include <iostream>
#include <string>

#include <tp5.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;


std::vector<string> TP5::names(
{
    "Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
    "JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
    "Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
    "Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
    "Fanny", "Jeanne", "Elo"
});

unsigned long int hash(string key)
{
    // return an unique hash id from key
    int i=0;
    unsigned long int hash_value=0;
    while (key[i] != '\0'){
        hash_value = hash_value + (int)key[i]*pow(128, key.size() - i);
        i++;
    }
    return hash_value;
}

struct MapNode : public BinaryTree
{

    string key;
    unsigned long int key_hash;

    int value;

    MapNode* left;
    MapNode* right;

    MapNode(string key, int value) : BinaryTree (value)
    {
        this->key = key;
        this->value = value;
        this->key_hash = hash(key);

        this->left = this->right = nullptr;
    }

    /**
     * @brief insertNode insert a new node according to the key hash
     * @param node
     */
    void insertNode(MapNode* node)
    {
        if (node->key_hash < this->key_hash){
            if (this->left!=NULL){ //Si le noeud à gauche n'est pas null alors on insert un nouveau noeud sinon le noeud gauche prend la valeur du noeud.
                this->left -> insertNode(node);
            }

            else {
                this->left=node;
            }
        }

        else {//same à droite mais pour la condition inverse du coup
            if (this->right!=NULL){
                this->right -> insertNode(node);
            }

            else {
                this->right=node;
            }
        }
    }

    void insertNode(string key, int value)
    {
        this->insertNode(new MapNode(key, value));
    }

    virtual ~MapNode() {}
    QString toString() const override {return QString("%1:\n%2").arg(QString::fromStdString(key)).arg(value);}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

struct Map
{
    Map() {
        this->root = nullptr;
    }

    /**
     * @brief insert create a node and insert it to the map
     * @param key
     * @param value
     */
    void insert(string key, int value)
    {
        if (this->root==NULL){
            this->root = new MapNode (key, value); //Si la racine est nulle alors on en créer une nouvelle.
        }
        else {
            this->root->insertNode(key, value);//Si la racine n'est pas nulle alors on peut insérer un nouveau noeud
        }
    }

    /**
     * @brief get return the value of the node corresponding to key
     * @param key
     * @return
     */
    int get(string key)
    {
        MapNode* element = this->root; //On commence en haut de l'arbre à la racine
        while (element != NULL){

            if (element->key_hash==hash(key)){ //Si la valeur de l'élement correspond à la valeur de hashage de key alors l'élement peut prendre la valeur correspondant à key
                return element->value;
            }

            if (element->key_hash<hash(key)){
                element=element->right;

            }

            else if (element->key_hash>hash(key)){
                element=element->left;

            }
        }
        return 0;

    }

    MapNode* root;
};


int main(int argc, char *argv[])
{
    srand(time(NULL));
	Map map;

    map.insert("Yolo", 20);
    for (std::string& name : TP5::names)
    {
        if (rand() % 3 == 0)
        {
            map.insert(name, rand() % 21);
        }
    }

    printf("map[\"Margot\"]=%d\n", map.get("Margot"));
    printf("map[\"Jolan\"]=%d\n", map.get("Jolan"));
    printf("map[\"Lucas\"]=%d\n", map.get("Lucas"));
    printf("map[\"Clemence\"]=%d\n", map.get("Clemence"));
    printf("map[\"Yolo\"]=%d\n", map.get("Yolo"));
    printf("map[\"Tanguy\"]=%d\n", map.get("Tanguy"));
    std::cout.flush();


    QApplication a(argc, argv);
    MainWindow::instruction_duration = 200;
    w = new MapWindow(*map.root);
    w->show();
    return a.exec();
}
