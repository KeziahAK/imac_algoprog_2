#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& result=w->newArray(toSort.size());
	result[0]=toSort[0];
    for (int i=1;i<toSort.size(); i++){
        for (int j=0; j<i; j++){
			if (toSort[i]<result[j]){
				result.insert(j, toSort[i]);
                break;
			
			}
			else {
                result.insert(toSort.size()-1, toSort[i]);
			}
		}
	}

	// insertion sort from toSort to sorted
	toSort=result; // update the original array
	
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
