#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;



void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	if (size==0)
	{
		return;
	}

	// split
	
	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);
	int lowerSize = 0, greaterSize = 0;
	int n=toSort[0];
	 // effectives sizes
	for (int i=1; i<size; i++){
		
		if (toSort[i]<n){
			lowerArray[lowerSize]=toSort[i];
			lowerSize=lowerSize+1;
		}
		else if (toSort[i]>n){
			greaterArray[greaterSize]=toSort[i];
			greaterSize=greaterSize+1;
		}
	}
	// recursiv sort of lowerArray and greaterArray
	recursivQuickSort(lowerArray, lowerSize);
	recursivQuickSort(greaterArray, greaterSize);
 	// merge
	for (int j=0; j<lowerSize; j++){
		toSort[j]=lowerArray[j];

	}
	toSort[lowerSize]=n;
	for (int j=lowerSize+1; j<size; j++){
		toSort[j] = greaterArray[j-lowerSize-1];

	}

   


	
}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
