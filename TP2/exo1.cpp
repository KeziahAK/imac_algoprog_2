#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
    
    for (int i=0; i<(toSort.size()); i++){
        int min=i;

        for (int j=i+1; j<(toSort.size()); j++){
            if (toSort[i]>toSort[j]){
                toSort[min]=toSort[j];//Si la valeur comparée est plus petite la prendre comme minimum
            }

        }

        toSort.swap(i, min);//Si i est plus petit alors on le swap avec le min

    }
	// selectionSort
}



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
