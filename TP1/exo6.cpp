#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    Noeud* dernier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int nombre_elts;
    int nb_elts_max;
    // your code
};


void initialise(Liste* liste)
{
    liste->premier=NULL;//on initialise une liste vide
    liste->dernier=NULL;
}

bool est_vide(const Liste* liste)
{
    return liste->premier==NULL;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* element = new Noeud;
    element-> donnee=valeur;
    if (liste->premier==NULL){
        liste->premier=element;
        liste->dernier=element;
    }
    else{
        liste->dernier->suivant=element;
        liste->dernier=liste->dernier->suivant;
    }


}

void affiche(const Liste* liste)
{
    if (est_vide(liste)){
        cout<<"pas de liste"<<endl;
    }
    Noeud* valeur_liste= liste->premier;
    while (valeur_liste != NULL){
        cout<< valeur_liste->donnee<<endl;
        valeur_liste=valeur_liste->suivant;//On pointe sur la valeur suivante pour avancer dans la liste
    }
}

int recupere(const Liste* liste, int n)
{   Noeud* element=liste->premier;
    int compteur=0;
    if (est_vide(liste)){
        cout<<"la liste est vide, aucune valeur à retourner"<<endl;
    }
    else{

        while (compteur!=n || element->suivant!=NULL){
            element=element->suivant;
            compteur=compteur+1;
        }
    }
    return element->donnee;
    
}

int cherche(const Liste* liste, int valeur)
{   int compteur=0;
    if (est_vide(liste)){
        cout<<"la liste est vide, aucune valeur à retourner"<<endl;
    }
    else{
        Noeud* element=liste->premier;
        
        while (element->donnee!=valeur){
            element=element->suivant;
            compteur++;
        }
    }
    return compteur;
    
}

void stocke(Liste* liste, int n, int valeur)
{   int compteur=0;
    Noeud* element=liste->premier;
    if (est_vide(liste)){
        cout<<"la liste est vide, aucune valeur à retourner"<<endl;
    }

    else {
        while (compteur!=n || element->suivant!=NULL){
            compteur++;
            element=element->suivant;
        } 
        element->donnee=valeur;
    }
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->nombre_elts>=tableau->nb_elts_max){
        tableau->nb_elts_max=tableau->nb_elts_max*2;
        tableau->donnees = (int*) realloc (tableau->donnees, sizeof(int) * tableau->nb_elts_max);
    }

    else {
        tableau->donnees[tableau->nombre_elts]=valeur;
        tableau->nombre_elts=tableau->nombre_elts+1;
    }
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->nb_elts_max=capacite;
    tableau->nombre_elts=0;
    tableau->donnees=(int*)malloc(sizeof(int)*capacite);

}

bool est_vide(const DynaTableau* liste)
{
    return liste->nombre_elts==0;
}

void affiche(const DynaTableau* tableau)
{
    if (est_vide(tableau)){
        cout<<"il n'y a rien à afficher"<<endl;
    }
    else {
        for (int i=0;i<=tableau->nombre_elts;i++){
            cout <<tableau->donnees[i]<<endl;
        }
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if(n>tableau->nombre_elts){
        cout << "la valeur que vous cherchez n'existe pas"<<endl;
        return 0;
    }
    else{
        return tableau->donnees[n-1];
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for (int i=0;i<=tableau->nombre_elts;i++){
        if (tableau->donnees[i]==valeur){
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    for (int i=0;i<=tableau->nombre_elts;i++){
        if (i==n){
            tableau->donnees[i]=valeur;}
    }
}


//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste,valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if (est_vide(liste)){
        return 0;
    }
    int valeur_retournee= 0;
    if (liste != nullptr && liste -> premier != nullptr) {
        valeur_retournee= liste->premier->suivant->donnee;
        Noeud* elementretire = liste->premier->suivant;
        delete elementretire;
        
    }
    return valeur_retournee;


}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    ajoute(liste,valeur);

}

//int retire_pile(DynaTableau* liste)
int retire_pile(DynaTableau* liste)
{
    if (est_vide(liste)){
        return 0;
    }
    
    else {
        int valeur = liste->donnees[liste->nombre_elts];
        liste->donnees[liste->nombre_elts] = NULL;
        liste->nombre_elts--;
        return valeur;    
    }


    
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&liste) && compteur > 0)
    {
        std::cout << retire_file(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_file(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
