#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot

    if (n==0){
        return 0;
    }

    else {
        //z^2=x^2 -y^2 +2xiy
        float reel= z.x;
        z.x=z.x*z.x +z.y*z.y + point.x; //partie réelle de f(z)
        z.y=2*z.x*z.y + point.y; //partie imaginaire de f(z)
        float modz = sqrt(pow(z.x,2)+pow(z.y,2)); //module de f(z)
        if (modz<2){
            return isMandelbrot(z,n-1,point);   
        }
        else {
            return 1;
        }
    }


    
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



